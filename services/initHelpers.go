package services

import (
	"bufio"
	"log"
	"os"
)

func GetValidWords(wordList string) []string {
	// assuming a single word list is in use might as well just load it once, or
	// even, as a possible refactor, just have it already formatted and read in as json.
	// That would probably be another endpoint, feed in a word list, create a big json
	// file of it by len, ready to be read in.

	f, err := os.Open(wordList)

	if err != nil {
		// if we had some logging service we should probably shout about this there
		// though realisitically if this happens you should see your gitlab deploy
		// or what have you red out and know you need to fix something.
		// just so as we progress through our tree search we can search smaller and smaller lists of words
		log.Fatal("Error reading wordlist:", err.Error())
	}

	defer f.Close()

	s := bufio.NewScanner(f)

	var validWords []string

	for s.Scan() {
		w := s.Text()

		validWords = append(validWords, w)
	}

	if err := s.Err(); err != nil {
		log.Fatal("Error scanning wordlist:", err.Error())
	}
	return validWords
}
