wordlist taken from https://github.com/first20hours/google-10000-english/blob/master/google-10000-english-no-swears.txt and words of less than 3 letters removed.![alt text](image.png)

Wordlist is arbitrary. A further extension might be allowing the user to submit their own wordlist or to select a particular word list. 

Runs on port 8080 by default but can be configured with the -port flag. 

![alt text](image-1.png)