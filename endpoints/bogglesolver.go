package endpoints

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"slices"

	"github.com/gin-gonic/gin"
)

// If these structs were used elsewhere I would move them to their own location but if something is only used in a single place
// it seems like a premature optimization that just gets you indirection for little gain.If the app were to grow though
// it might make sense to pull out the logic of the solver into a seperate service for easy reuse and just have the controller handle
// the req stuff. In that case both the endpoint and the service would need the type `boggleBoard` and it would make sense to make it Public and move
// it elsewhere. As is, creating a seperate service also seems needlessly obfacatory, it's internal logic is just not reused here.
// Perhaps an argument could be made for testing, to get some true TDD tests but in my experience making API's it is often fine to test
// their functionality at a request level, as that is how the logic within the controllers actually gets invoked.

type boardTile struct {
	Letter  string
	Visited bool
}

type boggleBoard struct {
	Board [][]boardTile `json:"board"`
}

type FoundWords struct {
	FoundWords []string `json:"foundWords"`
}

// Set the state of all tiles as unvisited
func (bt *boardTile) UnmarshalJSON(b []byte) error {

	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}

	*bt = boardTile{
		Letter:  strings.ToLower(s),
		Visited: false,
	}
	return nil
}

func wordFinder(board [][]boardTile, validWords []string, foundWords []string) []string {

	// we could pull this out I just like it for the closure
	var wordFindHelper func([][]boardTile, string, int, int, int)
	wordFindHelper = func(boardToTraverse [][]boardTile, searchedWord string, lookingForCharAt int, row int, col int) {
		boardToTraverse[row][col].Visited = true
		letter := boardToTraverse[row][col].Letter

		// the letter at this coordinate is not the letter that we need next to complete the word...
		if string(searchedWord[lookingForCharAt]) != letter {
			return
		} else if lookingForCharAt == len(searchedWord)-1 { //we have found the last letter, add it to found words if not present
			if !slices.Contains(foundWords, searchedWord) {
				foundWords = append(foundWords, searchedWord)
			}
			return
		} else {
			//check all adjacent non visited cells to see if the next letter can be found
			for _, rowOffset := range []int{-1, 0, 1} {
				for _, colOffset := range []int{-1, 0, 1} {
					newRow := row + rowOffset
					newCol := col + colOffset
					if (rowOffset != 0 || colOffset != 0) &&
						(newRow >= 0 && newRow < len(boardToTraverse)) &&
						(newCol >= 0 && newCol < len(boardToTraverse)) &&
						!boardToTraverse[newRow][newCol].Visited { //order matters!
						if lookingForCharAt+1 == len(searchedWord) { //at the end, return
							return
						}
						wordFindHelper(boardToTraverse, searchedWord, lookingForCharAt+1, row+rowOffset, col+colOffset)
					}
				}
			}
		}

	}

	for _, word := range validWords {
		for row := range board {
			for col := range board[0] {
				var boardCopy [][]boardTile
				for _, row := range board {
					copyRow := make([]boardTile, len(board))
					copy(copyRow, row)
					boardCopy = append(boardCopy, copyRow)
				}
				if slices.Contains(foundWords, word) {
					continue
				}
				wordFindHelper(boardCopy, word, 0, row, col)
			}
		}
	}

	return foundWords

}

func PostBoggleSolver(validWords []string) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		var boardReq boggleBoard
		jsonData, err := io.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"Invalid Request:": err.Error()})
			return
		}
		// c.Request.Body
		// if err := c.ShouldBindJSON(&boardReq); err != nil {
		if err := json.Unmarshal(jsonData, &boardReq); err != nil {
			// if we had more endpoints a place where we stored our error resp objs would be good but like above,
			// with a single endpoint it gets us little to abstract this.
			c.JSON(http.StatusBadRequest, gin.H{"Invalid Request:": err.Error()})
			return
		}

		board := boardReq.Board

		if len(board) != len(board[0]) || len(board[0]) < 2 {
			c.JSON(http.StatusBadRequest, gin.H{"Invalid Board:": "board must be at least 3x3 and square."})
			return
		}

		wordsToReturn := wordFinder(board, validWords, []string{})

		c.JSON(http.StatusOK, wordsToReturn)
	}

	return gin.HandlerFunc(fn)

}
