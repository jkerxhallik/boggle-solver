package endpoints

import (
	"bytes"
	"encoding/json"
	"jkk/boggle_solver/services"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert"
)

// Found that as these things grow you often want some custom app setupt for tests,
// e.g. you might want to load a different word list here, or, from something I previously
// worked on, load in a config file from a different location than normal.

func SetupTestRouter() *gin.Engine {
	router := gin.Default()

	validWords := services.GetValidWords("../wordlists/test-valid-words.txt")

	// validWords := services.GetValidWords("../wordlists/google-9474-english-no-swears-3-plus-letters.txt")

	router.POST("/boggle-solver", PostBoggleSolver(validWords))

	return router
}

// Not technically a unit test but exersises the relevant code paths.
// If we had more complicated logic for solving we might hoist it out and test it
// individually then.

func TestPostBoggleSolverValidInputs(t *testing.T) {

	testCases := []struct {
		fixtureFile    string
		expectedOutput []string
	}{
		{"../fixtures/testpost1.json", []string{"the", "ten", "hen", "ent"}},
		{"../fixtures/testpost2.json", []string{"the", "ten", "hen", "ent"}},
	}

	r := SetupTestRouter()

	for _, tc := range testCases {

		w := httptest.NewRecorder()

		reqBody, _ := os.ReadFile(tc.fixtureFile)

		req, _ := http.NewRequest("POST", "/boggle-solver", bytes.NewBuffer((reqBody)))

		r.ServeHTTP(w, req)

		assert.Equal(t, 200, w.Code)

		var postResp []string

		json.Unmarshal(w.Body.Bytes(), &postResp)

		// possible responses could get pretty long, could be worth having a file with pre-computed answers
		// for the second arg.
		assert.Equal(t, tc.expectedOutput, postResp)

	}
}
