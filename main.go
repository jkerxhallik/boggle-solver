package main

import (
	"flag"
	"jkk/boggle_solver/endpoints"
	"jkk/boggle_solver/services"

	"github.com/gin-gonic/gin"
)

func main() {

	port := flag.String("port", "8080", "Port to run the app on. Default 8080")
	flag.Parse()

	router := SetupRouter()

	router.Run(":" + *port)

}
func SetupRouter() *gin.Engine {
	router := gin.Default()

	validWords := services.GetValidWords("./wordlists/google-9474-english-no-swears-3-plus-letters.txt")

	router.POST("/boggle-solver", endpoints.PostBoggleSolver(validWords))

	return router
}
